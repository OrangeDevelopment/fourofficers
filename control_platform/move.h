#ifndef MOVE
#define MOVE

#include <QObject>
#include <QDebug>
#include <QSettings>
#include <qmodbus.h>

class Control:public QObject
{
    Q_OBJECT
public:
    enum TypeJoystick{X_Y,Y_X,_XY,XY_};
    Control(QObject* parent=0);
    ~Control();
    void readSettings();
    void writeSettings();
public slots:
    void Y(double y);
    void X(double x);
    void XY(double x,double y);
    void stop_Y();
    void stop_X();
    void stop_XY();

    int getLeft() {return left;}
    int getRight() {return right;}
    void setType(int _left,int _right);
    QString getIP() {return hostIP;}
    void setIP(QString _hostIP) {hostIP = _hostIP;}

    void init();

    void onInit();
    void onConnected();
    void onPoll();
    void onError(int code, QString message);
    void onCoilChanged(int coilNubler, bool newState);
    void onInputChanged(int inputNumber, bool newState);

signals:
    void sendConnected(int status);
    void initStick(int left, int right);
private:
    //QSettings settings;
    QModBus *mb;
    int left;
    int right;
    int status;
    QString hostIP;
};

#endif // MOVE
