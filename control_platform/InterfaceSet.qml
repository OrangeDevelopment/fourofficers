import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4


Rectangle {
    id: interfaceSet
    signal signalExit
    signal changeType(int leftStick, int rightStick)
    visible: false
    color: "darkgray"
    opacity: 0.7
    width: root.width/2
    height: dp(200)
    radius: 7
    anchors.horizontalCenter: parent.horizontalCenter
    Rectangle {
        id: exit_frag1
        x: interfaceSet.width-exit_frag1.width
        width: dp(40)
        height: dp(40)
        color: button_green_mouse_area.pressed ? "darkred" : "red"
        radius: 7
        Text {
            id: button_green_label
            text: qsTr("Х")
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 16
        }
        MouseArea {
            id: button_green_mouse_area
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                listType.visible = false
                interfaceSet.signalExit()
            }
        }
    }
    Rectangle {
        id: selectType
        width: interfaceSet.width/1.5
        height: interfaceSet.height / 5
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        color: "transparent"
        anchors.margins: 20
        Rectangle {
            id: textType
            width: parent.width / 2
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            color: "transparent"
            Text {
                font.pixelSize: dp(18)
                anchors.right: parent.right
                anchors.rightMargin: dp(20)
                anchors.verticalCenter: parent.verticalCenter
                renderType: Text.NativeRendering
                text: "Тип управления"
            }
        }
        Rectangle {
            id: changeType
            width: parent.width / 2
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            color: "transparent"
            ListModel{
                id: typeModel
                ListElement{ image: "type1.png"; leftStick: 1; rightStick: 2}
                ListElement{ image: "type2.png"; leftStick: 2; rightStick: 1}
                ListElement{ image: "type3.png"; leftStick: 0; rightStick: 3 }
                ListElement{ image: "type4.png"; leftStick: 3; rightStick: 0 }
            }
            Rectangle {
                id: comboButton
                anchors.leftMargin: dp(20)
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width * 3 / 4
                height: parent.height
                radius: 5
                border.width: 2
                color: areaType.pressed ? "#888" : "#fff"
                Image {
                    id: imageType
                    width: comboButton.width
                    height: comboButton.height
                    source:
                        switch(move.getLeft())
                        {
                        case 0: imageType.source = "type3.png"
                            break;
                        case 1: imageType.source = "type1.png"
                            break;
                        case 2: imageType.source = "type2.png"
                            break;
                        case 3: imageType.source = "type4.png"
                            break;
                        default:100;
                            break;
                        }//"type1.png"
                    fillMode: Image.PreserveAspectFit
                }
                MouseArea {
                    id: areaType
                    anchors.fill: parent
                    onClicked: {
                        if(listType.visible)
                            listType.visible = false
                        else
                            listType.visible = true
                    }
                }
            }
            ListView{
                id: listType
                visible: false
                model: typeModel
                anchors.top: comboButton.bottom
                anchors.left: comboButton.left
                anchors.right: comboButton.right
                height: comboButton.height * 4
                delegate: Rectangle {
                    id: rect
                    width: comboButton.width
                    height: comboButton.height
                    border.width: 0.5
                    border.color: "black"
                    Image {
                        width: parent.width
                        height: parent.height
                        source: image
                        fillMode: Image.PreserveAspectFit
                    }
                    MouseArea {
                        id: clickArea
                        anchors.fill: parent
                        onClicked: {
                            listType.visible = false
                            imageType.source = image
                            interfaceSet.changeType(leftStick,rightStick)
                        }
                    }
                }
            }
        }
    }
}
