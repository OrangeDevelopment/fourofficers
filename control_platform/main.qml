import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Particles 2.0
import QtQuick.Controls.Styles 1.1

ApplicationWindow {
    id: main
    signal returnMenu()
    signal initStick()
    //visibility: "FullScreen"

    width: 800//Screen.width//800
    height: 480//Screen.height//480
    readonly property int dpi: Screen.pixelDensity * 25.4
    function dp(x){ return (dpi < 120) ? x : x*(dpi/160); }
    visible: true
    title: qsTr("Control platform")

    Component {
        id: control
        Control {
            onMenuBack: {
                loader.sourceComponent = undefined
                loader.sourceComponent = menuView
            }
        }
    }
    Component {
        id: menuView
        MenuView {
            onSelectView: {
                loader.sourceComponent = undefined
                if(choice==="control")
                {
                    loader.sourceComponent = control
                    main.initStick()
                }
                if(choice==="exit")
                    Qt.quit()
            }
        }
    }
    Loader {
        property int vision: 0
        id: loader
        asynchronous: true
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width
        height: parent.height
        sourceComponent: menuView
        onLoaded: {
            if(loader.sourceComponent===control)
            {
                move.onInit()
            }
        }
    }
}
