import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Particles 2.0
import QtQuick.Controls.Styles 1.1
import QtGStreamer 1.0

Rectangle {
    signal menuBack
    id: root
    width: 800//Screen.width//800
    height: 480//Screen.height//480
    property int widht_stick: form.height
//*
    VideoItem {
        id: video

        width: root.width
        height: root.height
        surface: videoSurface1 //bound on the context from main()
        Rectangle{
            width: root.width/4
            height: root.height/4
            x: (root.width/2)-width/2
            y:0
            border.width: 3
            border.color: "black"
            VideoItem {
                id: video2
                width: parent.width
                height: parent.height
                surface: videoSurface2 //bound on the context from main()
            }

        }
    }
//*/
    Connections {
        target: move
        onSendConnected: {
            connectionForm.color = getColor(status)
        }
        onInitStick: {
            model.setProperty(0,"type",left)
            model.setProperty(1,"type",right)
        }
    }

    function getColor(x){ return (x === 0) ? "red" : "green"; }
    readonly property int dpi: Screen.pixelDensity * 25.4
    function dp(x){ return (dpi < 120) ? x : x*(dpi/160); }
    Rectangle {
        anchors.fill: parent
        color: "transparent"

        Rectangle {
        id: menuRect
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: dp(48)
        color: "transparent"

        Rectangle {
            id: hamb
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left

            width: dp(48)
            color: "transparent"

            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(16)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
                color: "black"
            }

            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(23)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
                color: "black"
            }

            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(30)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
                color: "black"
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    nav.toggle()
                }
            }
        }
       Rectangle {
           id: connectionForm
           color: "red"
           anchors.top: parent.top
           anchors.bottom: parent.bottom
           anchors.right: parent.right
           width: menuRect.height
       }
    }
    InterfaceSet {
        id: interfaceSet
        onSignalExit: {
            interfaceSet.visible=false
        }
        onChangeType: {
            move.setType(leftStick,rightStick)
            model.setProperty(0,"type",move.getLeft())
            model.setProperty(1,"type",move.getRight())
        }
    }
    NetworkSet {
        id: networkSet
        onSignalExit: {
            networkSet.visible=false
        }
    }

    Loader {
        property int vision: 0
        id: loader
        asynchronous: true
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        function loadSize(index){
            switch(index){
            case 0:
                loader.width = 200
                loader.height = 200
                break;
            case 1:
                loader.width = root.width-300
                loader.height = 200
                break;
            case 2:
                loader.width = 200
                loader.height = 200
                break;
            }
        }
        function loadFragment(index){

            switch(index){
            case 0:
                interfaceSet.visible=true
                break;
            case 1:
                networkSet.visible=true
                break;
            case 2:

                break;
            }
            nav.toggle()

        }
    }
    NavigationDrawer {
        id: nav
        Rectangle {
            anchors.fill: parent
            // Список с пунктами меню

            ListView {
                anchors.fill: parent
                delegate: Item {
                    height: dp(48)
                    anchors.left: parent.left
                    anchors.right: parent.right

                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: dp(5)
                        color: "whitesmoke"

                        Text {
                            text: fragment
                            anchors.fill: parent
                            font.pixelSize: dp(20)

                            renderType: Text.NativeRendering
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }

                        MouseArea {
                            anchors.fill: parent
                            // По нажатию на пункт меню заменяем компонент в Loader
                            onClicked: {
                                if (fragment==="Главное меню") root.menuBack()
                                loader.loadSize(index)
                                loader.loadFragment(index)
                            }
                        }
                    }
                }
                model: navModel
            }
        }
    }
    ListModel {
        id: navModel
        ListElement {fragment: "Интерфейс"}
        ListElement {fragment: "Сеть"}
        ListElement {fragment: "Главное меню"}
    }
    Rectangle {
        id: form

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: root.height / 2
        color: "transparent"
        ListModel {
            id: model
            ListElement {
                type: 0
            }
            ListElement {
                type: 0
            }
        }
        GridView {
            id: view
            interactive: false
            cellHeight: form.height
            cellWidth: root.width/2
            anchors.fill: parent
            clip: true
            model: model
            delegate: Rectangle {
                height: view.height
                width: view.width
                color: "transparent"
                Rectangle {
                    width: root.widht_stick
                    height: width
                    color: "transparent"
                    x: (view.width/4) - width/2
                    JoystickForm {
                        property int type_stick: model.type
                    }
                }
            }
        }

    }
    }
}

