import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

Item {
    anchors.fill: parent

Item {
    property alias joystick: joystick
    property int type: parent.type_stick
    id: joystick
    x: (root.widht_stick - joystick.width)/2
    y: (form.height - joystick.height) / 2
    width:
        switch(joystick.type)
        {
        case 0: joystick.visible = false;
            break;
        case 1: joystick.visible = true ;root.widht_stick / 5
            break;
        case 2: joystick.visible = true; root.widht_stick - 100
            break;
        case 3: joystick.visible = true; root.widht_stick - 100
            break;
        default:100;
            break;
        }
    height:
        switch(joystick.type)
        {
        case 0: joystick.visible = false;
            break;
        case 1: joystick.visible = true; form.height - 100
            break;
        case 2: joystick.visible = true; form.height / 5
            break;
        case 3: joystick.visible = true; root.widht_stick - 100
            break;
        default:100;
            break;
        }
    property double bwidth:joystick.width/20
    property double bheight:joystick.height/20
    property alias pressed: mouseArea.focus
    Rectangle {
        anchors.fill: parent
        color:"transparent"
        border.width: 3
        border.color: "#1e1b18"
        radius: width / 2
    }

    Rectangle {
        id: handle
        width:
           switch(joystick.type)
           {
           case 1: joystick.width + 10
               break;
           case 2: joystick.height + 10
               break;
           case 3: joystick.width / 3
               break;
           case 4: joystick.width / 3
               break;
           default: 100;
               break;
           }
        height: width
        anchors.centerIn: joystick

        anchors.onHorizontalCenterOffsetChanged:
            anchors.horizontalCenterOffset / (joystick.width / 2 - handle.width / 2)
        anchors.onVerticalCenterOffsetChanged:
            anchors.verticalCenterOffset / (joystick.height / 2 - handle.height / 2)
        radius: width / 2
        color: "#717171"
    }
    NumberAnimation {
        id: returnAnimation
        target: handle.anchors
        properties: "horizontalCenterOffset,verticalCenterOffset"
        to: 0
        duration: 200
        easing.type: Easing.OutSine
    }
    MultiPointTouchArea {
        id: mouseArea
        touchPoints: [
            TouchPoint { id: touch }
        ]
        anchors.fill: parent
        property int startX
        property int startY
        property bool fingerOutOfBounds:
            switch(joystick.type)
            {
                case 1:
                    (touch.x - startX) * (touch.x - startX) + (touch.y-startY) * (touch.y-startY) < (joystick.height / 2) * (joystick.height / 2 )
                    break;
                case 2:
                    (touch.x - startX) * (touch.x - startX) + (touch.y-startY) * (touch.y-startY) < (joystick.width / 2) * (joystick.width / 2)
                    break;
                case 3:
                    (touch.x - startX) * (touch.x - startX) + (touch.y-startY) * (touch.y-startY) < (joystick.width / 2 ) * (joystick.width / 2)
                    break;
                case 4:
                    (touch.x - startX) * (touch.x - startX) + (touch.y-startY) * (touch.y-startY) < (joystick.width / 2 ) * (joystick.width / 2)
                    break;
                default:
                    (touch.x - startX) * (touch.x - startX) + (touch.y-startY) * (touch.y-startY) < (joystick.width / 2 ) * (joystick.width / 2)
                    break;
            }
        onPressed: {
            startX = touch.x
            startY = touch.y
            joystick.anchors.horizontalCenterOffset = touch.x - width / 2
            joystick.anchors.verticalCenterOffset = touch.y - height / 2

        }
        onUpdated: {
            switch(joystick.type)
            {
            case 1:
                if (fingerOutOfBounds) {
                    handle.anchors.verticalCenterOffset = touch.y - startY
                    move.Y((handle.y - joystick.height/2 + handle.height/2)/joystick.bheight)
                }
                else {
                    var angle = Math.atan2(touch.y - startY, touch.x - startX)
                    handle.anchors.verticalCenterOffset = Math.sin(angle) * (joystick.height / 2)
                    move.Y((handle.y - joystick.height/2 + handle.height/2)/joystick.bheight)
                }
                break;
            case 2:
                if (fingerOutOfBounds) {
                    handle.anchors.horizontalCenterOffset = touch.x - startX
                    move.X((handle.x - joystick.width/2 + handle.height/2)/joystick.bwidth)
                }
                else {
                    var angle = Math.atan2(touch.y - startY, touch.x - startX)
                    handle.anchors.horizontalCenterOffset = Math.cos(angle) * (joystick.width/ 2)
                    move.X((handle.x - joystick.width/2 + handle.height/2)/joystick.bwidth)
                }
                break;
            case 3:
                if (fingerOutOfBounds) {
                    handle.anchors.horizontalCenterOffset = touch.x - startX
                    handle.anchors.verticalCenterOffset = touch.y - startY
                    move.XY((handle.x - joystick.width/2 + handle.height/2)/joystick.bwidth,(handle.y - joystick.height/2 + handle.height/2)/joystick.bheight)
                } else {
                    var angle = Math.atan2(touch.y - startY, touch.x - startX)
                    handle.anchors.horizontalCenterOffset = Math.cos(angle) * (joystick.width / 2)
                    handle.anchors.verticalCenterOffset = Math.sin(angle) * (joystick.width / 2)
                    move.XY((handle.x - joystick.width/2 + handle.height/2)/joystick.bwidth,(handle.y - joystick.height/2 + handle.height/2)/joystick.bheight)
                }
                break;
            case 4:
                if (fingerOutOfBounds) {
                    handle.anchors.horizontalCenterOffset = touch.x - startX
                    handle.anchors.verticalCenterOffset = touch.y - startY
                    move.XY((handle.x - joystick.width/2 + handle.height/2)/joystick.bwidth,(handle.y - joystick.height/2 + handle.height/2)/joystick.bheight)
                } else {
                    var angle = Math.atan2(touch.y - startY, touch.x - startX)
                    handle.anchors.horizontalCenterOffset = Math.cos(angle) * (joystick.width / 2)
                    handle.anchors.verticalCenterOffset = Math.sin(angle) * (joystick.width / 2)
                    move.XY((handle.x - joystick.width/2 + handle.height/2)/joystick.bwidth,(handle.y - joystick.height/2 + handle.height/2)/joystick.bheight)
                }
                break;
            default:
                if (fingerOutOfBounds) {
                    handle.anchors.horizontalCenterOffset = touch.x - startX
                    handle.anchors.verticalCenterOffset = touch.y - startY
                    move.XY((handle.x - joystick.width/2 + handle.height/2)/joystick.bwidth,(handle.y - joystick.height/2 + handle.height/2)/joystick.bheight)
                } else {
                    var angle = Math.atan2(touch.y - startY, touch.x - startX)
                    handle.anchors.horizontalCenterOffset = Math.cos(angle) * (joystick.width / 2)
                    handle.anchors.verticalCenterOffset = Math.sin(angle) * (joystick.width / 2)
                    move.XY((handle.x - joystick.width/2 + handle.height/2)/joystick.bwidth,(handle.y - joystick.height/2 + handle.height/2)/joystick.bheight)
                }
                break;
            }
        }
        onReleased: {
            switch(joystick.type)
            {
            case 1: move.stop_Y()
                break;
            case 2: move.stop_X()
                break;
            case 3: move.stop_XY()
                break;
            }
            returnAnimation.start()
        }
    }
}
}
