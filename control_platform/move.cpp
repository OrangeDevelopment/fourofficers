#include "move.h"

#define _Y_ 1
#define _X_ 2

Control::Control(QObject *parent):QObject(parent)
{
    readSettings();
    mb = new QModBus(this);
    connect(mb,SIGNAL(connected()),this,SLOT(onConnected()));
    connect(mb,SIGNAL(poll()),this,SLOT(onPoll()));
    connect(mb,SIGNAL(coilChanged(int,bool)),this,SLOT(onCoilChanged(int,bool)));
    connect(mb,SIGNAL(inputChanged(int,bool)),this,SLOT(onInputChanged(int,bool)));
    connect(mb,SIGNAL(error(int,QString)),this,SLOT(onError(int,QString)));
    init();
    //mb->init(hostIP,12,12,12,1502);
    mb->startPoll(5000);
}
void Control::readSettings()
{
    QSettings* settings=new QSettings("Settings.ini",QSettings::IniFormat);
    left = settings->value("Setting/left",1).toInt();
    right = settings->value("Setting/right",2).toInt();
    hostIP = settings->value("Setting/hostIP","192.168.2.17").toString();
}

void Control::init()
{
    mb->init(hostIP,12,12,12,1502);
}

void Control::onInit()
{
    emit initStick(left,right);
}

void Control::onConnected()
{
    status = 1;
    emit sendConnected(status);
    qDebug()<<"Connected!";
}

void Control::onPoll()
{
    qDebug()<<"Polling";
    //mb->setCoil(6,true);
}

void Control::onError(int code, QString message)
{
    status = 0;
    emit sendConnected(status);
    //qDebug()<<"ERROR! Code: "<<code<<"Message"<<message;
    //формировать нули в пакетах если сеть упала
    //mb->init(hostIP,12,12,12,1502);
    init();
}

void Control::onCoilChanged(int coilNubler, bool newState)
{
    qDebug()<<"Coil:"<<coilNubler<<" changed to:"<< newState;
}

void Control::onInputChanged(int inputNumber, bool newState)
{
    qDebug()<<"Input: "<<inputNumber<<" changed to:"<< newState;
}

void Control::Y(double y)
{
    if(status)
    {
        int _y = (qRound(y)/2)*(-1);
        _y+=5;
        mb->setRegister(_Y_,_y);
        qDebug() << "Y = " << _y << "X = " << mb->getRegisterValue(_X_);
    }
}
void Control::X(double x)
{
    if(status)
    {
        int _x = qRound(x)/2;
        _x+=5;
        mb->setRegister(_X_,_x);
        qDebug() << "Y = " << mb->getRegisterValue(_Y_) << "X = " << _x;
    }
}

void Control::setType(int _left, int _right)
{
     left = _left;
     right = _right;
}

void Control::XY(double x,double y)
{
    if(status)
    {
        int _x = qRound(x)/2;
        int _y = (qRound(y)/2)*(-1);
        _x+=5;
        _y+=5;
        mb->setRegister(_X_,_x);
        mb->setRegister(_Y_,_y);
        qDebug() << "X = " << _x << "Y = " << _y;
    }
}

void Control::stop_Y()
{
    mb->setRegister(_Y_,5);
    qDebug() << "Y_STOP";
}

void Control::stop_X()
{
    mb->setRegister(_X_,5);
    qDebug() << "X_STOP";
}

void Control::stop_XY()
{
    mb->setRegister(_X_,5);
    mb->setRegister(_Y_,5);
    qDebug() << "XY_STOP";
}

void Control::writeSettings()
{
    QSettings* settings=new QSettings("Settings.ini",QSettings::IniFormat);
    settings->setValue("Setting/left",left);
    settings->setValue("Setting/right",right);
    settings->setValue("Setting/hostIP",hostIP);
    settings->sync();
}

Control::~Control()
{
    writeSettings();
}
