import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
Item {
Item {
    property alias joystickLeft: joystickLeft
    id: joystickLeft
    x:
    switch(root.type_left)
    {
    case 1: 0;
        break;
    case 2: 0;
        break;
    case 3:100
        break;
    default:100;
        break;
    }
    y:
    switch(root.type_left)
    {

    case 1: 0
        break;
    case 2: (root.height / 3) / 2 - handleLeft.height / 2;
        break;
    case 3:100
        break;
    default:100;
        break;
    }
    width:
        switch(root.type_left)
        {
        case 0:joystickLeft.visible = false;
            break;
        case 1: root.width / 20
            break;
        case 2: root.width / 4.5
            break;
        case 3:100
            break;
        default:
            100; joystickLeft.visible = false;
            break;
        }
    height:
        switch(root.type_left)
        {
        case 1: root.height / 3
            break;
        case 2: root.height / 11
            break;
        case 3:100
            break;
        default:100;
            break;
        }

    property alias pressed: mouseAreaLeft.pressed
    Rectangle {
        anchors.fill: parent

        border.width: 3
        border.color: "#1e1b18"
        radius: width / 2
    }

    Rectangle {
        id: handleLeft
        width:
           switch(root.type_left)
           {
           case 1: joystickLeft.width + 10
               break;
           case 2: joystickLeft.height + 10
               break;
           case 3:100
               break;
           default:100;
               break;
           }
        height: width
        anchors.centerIn: joystickLeft
        anchors.onHorizontalCenterOffsetChanged:
            anchors.horizontalCenterOffset / (joystickLeft.width / 2 - handleLeft.width / 2)
        anchors.onVerticalCenterOffsetChanged:
            anchors.verticalCenterOffset / (joystickLeft.height / 2 - handleLeft.height / 2)
        radius: width / 2
        color: "#717171"
    }

    MouseArea {
        id: mouseAreaLeft
        property alias mouseAreaLeft: mouseAreaLeft
        anchors.fill: parent
        property int startX
        property int startY
        property bool fingerOutOfBounds:
            switch(type_left)
            {
                case 1:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickLeft.height / 2) * (joystickLeft.height / 2 )
                    break;
                case 2:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickLeft.width / 2) * (joystickLeft.width / 2)
                    break;
                case 3:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickLeft.width / 2 ) * (joystickLeft.width / 2)
                    break;
                default:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickLeft.width / 2 ) * (joystickLeft.width / 2)
                    break;
            }
        //onPressed:
        //startX = mouseX
        //startY = mouseY
        //joystickLeft.anchors.horizontalCenterOffset = mouseX - width / 2
        //joystickLeft.anchors.verticalCenterOffset = mouseY - height / 2
        /*
        onPositionChanged: {
            switch(joystickLeft.type)
            {
            case 1:
                if (fingerOutOfBounds) {
                    handleLeft.anchors.verticalCenterOffset = mouseY - startY
                    asd.Y((handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                }
                else {
                    var angle = Math.atan2(mouseY - startY, mouseX - startX)
                    handleLeft.anchors.verticalCenterOffset = Math.sin(angle) * (joystickLeft.height / 2)
                    asd.Y((handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                }
                break;
            case 2:
                if (fingerOutOfBounds) {
                    handleLeft.anchors.horizontalCenterOffset = mouseX - startX
                    asd.X((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth)
                }
                else {
                    var angle = Math.atan2(mouseY - startY, mouseX - startX)
                    handleLeft.anchors.horizontalCenterOffset = Math.cos(angle) * (joystickLeft.width/ 2)
                    asd.X((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth)
                }
                break;
            case 3:
                if (fingerOutOfBounds) {
                    handleLeft.anchors.horizontalCenterOffset = mouseX - startX
                    handleLeft.anchors.verticalCenterOffset = mouseY - startY
                    asd.XY((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth,(handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                } else {
                    var angle = Math.atan2(mouseY - startY, mouseX - startX)
                    handleLeft.anchors.horizontalCenterOffset = Math.cos(angle) * (joystickLeft.width / 2)
                    handleLeft.anchors.verticalCenterOffset = Math.sin(angle) * (joystickLeft.width / 2)
                    asd.XY((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth,(handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                }
                break;
            default:
                if (fingerOutOfBounds) {
                    handleLeft.anchors.horizontalCenterOffset = mouseX - startX
                    handleLeft.anchors.verticalCenterOffset = mouseY - startY
                    asd.XY((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth,(handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                } else {
                    var angle = Math.atan2(mouseY - startY, mouseX - startX)
                    handleLeft.anchors.horizontalCenterOffset = Math.cos(angle) * (joystickLeft.width / 2)
                    handleLeft.anchors.verticalCenterOffset = Math.sin(angle) * (joystickLeft.width / 2)
                    asd.XY((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth,(handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                }
                break;
            }
        }
        onReleased: {
            console.log("1")
            asd.stop();
            returnAnimationLeft.start()
        }
        */
    }

}
Item {
    property alias joystickRight: joystickRight
    id: joystickRight
    x:
    switch(root.type_right)
    {
    case 1: root.width - root.width / 20 - 150;
        break;
    case 2: root.width - root.width / 20 - 300;
        break;
    case 3:100
        break;
    default:100;
        break;
    }
    y:
    switch(root.type_right)
    {
    case 1: 0
        break;
    case 2: (root.height / 3) / 2 - handleRight.height / 2;
        break;
    case 3:100
        break;
    default:100;
        break;
    }
    width:
        switch(root.type_right)
        {
        case 0:joystickLeft.visible = false;
            break;
        case 1: root.width / 20
            break;
        case 2: root.width / 4.5
            break;
        case 3:100
            break;
        default:
            100; joystickLeft.visible = false;
            break;
        }
    height:
        switch(root.type_right)
        {
        case 1: root.height / 3
            break;
        case 2: root.height / 11
            break;
        case 3:100
            break;
        default:100;
            break;
        }

    property alias pressed: mouseAreaRight.pressed
    Rectangle {
        anchors.fill: parent
        border.width: 3
        border.color: "#1e1b18"
        radius: width / 2
    }

    Rectangle {
        id: handleRight
        width:
           switch(root.type_left)
           {
           case 1: joystickLeft.width + 10
               break;
           case 2: joystickLeft.height + 10
               break;
           case 3:100
               break;
           default:100;
               break;
           }
        height: width
        anchors.centerIn: joystickRight
        anchors.onHorizontalCenterOffsetChanged:
            anchors.horizontalCenterOffset / (joystickRight.width / 2 - handleRight.width / 2)
        anchors.onVerticalCenterOffsetChanged:
            anchors.verticalCenterOffset / (joystickRight.height / 2 - handleRight.height / 2)
        radius: width / 2
        color: "#717171"
    }

    MouseArea {
        id: mouseAreaRight
        anchors.fill: parent
        property int startX
        property int startY
        property bool fingerOutOfBounds:
            switch(type_right)
            {
                case 1:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickRight.height / 2) * (joystickRight.height / 2 )
                    break;
                case 2:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickRight.width / 2) * (joystickRight.width / 2)
                    break;
                case 3:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickRight.width / 2 ) * (joystickRight.width / 2)
                    break;
                default:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickRight.width / 2 ) * (joystickRight.width / 2)
                    break;
            }
    }
}
}
