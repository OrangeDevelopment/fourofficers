import QtQuick 2.4
import QtQuick.Window 2.2
import QtSensors 5.3

Item {
    property int type: 1/*1-Y 2-X 3-XY */
    x:60
    y:40
    id: joystickLeft
    width:
        switch(joystickLeft.type)
        {
        case 0:joystickLeft.visible = false;
            break;
        case 1:40;
            break;
        case 2:140
            break;
        case 3:100
            break;
        default:
            100; joystickLeft.visible = false;
            break;
        }
    height:
        switch(joystickLeft.type)
        {
        case 1:140
            break;
        case 2:40
            break;
        case 3:100
            break;
        default:100;
            break;
        }

    property double bwidth:joystickLeft.width/20
    property double bheight:joystickLeft.height/20
    property alias pressed: mouseAreaLeft.pressed
    Rectangle {
        anchors.fill: parent
        border.width: 3
        border.color: "#1e1b18"
        radius: width / 2
    }

    Rectangle {
        id: handleLeft
        width: 50
        height: 50
        anchors.centerIn: joystickLeft
        anchors.onHorizontalCenterOffsetChanged:
            anchors.horizontalCenterOffset / (joystickLeft.width / 2 - handleLeft.width / 2)
        anchors.onVerticalCenterOffsetChanged:
            anchors.verticalCenterOffset / (joystickLeft.height / 2 - handleLeft.height / 2)
        radius: width / 2
        color: "#717171"
    }

    NumberAnimation {
        id: returnAnimationLeft
        target
        : handleLeft.anchors
        properties: "horizontalCenterOffset,verticalCenterOffset"
        to: 0
        duration: 200
        easing.type: Easing.OutSine

    }
    MouseArea {
        id: mouseAreaLeft
        anchors.fill: parent
        property int startX
        property int startY
        property bool fingerOutOfBounds:
            switch(joystickLeft.type)
            {
                case 1:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickLeft.height / 2) * (joystickLeft.height / 2 )
                    break;
                case 2:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickLeft.width / 2) * (joystickLeft.width / 2)
                    break;
                case 3:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickLeft.width / 2 ) * (joystickLeft.width / 2)
                    break;
                default:
                    (mouseX - startX) * (mouseX - startX) + (mouseY-startY) * (mouseY-startY) < (joystickLeft.width / 2 ) * (joystickLeft.width / 2)
                    break;
            }
        onPressed: {
            startX = mouseX
            startY = mouseY
            joystickLeft.anchors.horizontalCenterOffset = mouseX - width / 2
            joystickLeft.anchors.verticalCenterOffset = mouseY - height / 2
        }
        onPositionChanged: {
            switch(joystickLeft.type)
            {
            case 1:
                if (fingerOutOfBounds) {
                    handleLeft.anchors.verticalCenterOffset = mouseY - startY
                    //asd.Y((handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                }
                else {
                    var angle = Math.atan2(mouseY - startY, mouseX - startX)
                    handleLeft.anchors.verticalCenterOffset = Math.sin(angle) * (joystickLeft.height / 2)
                    //asd.Y((handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                }
                break;
            case 2:
                if (fingerOutOfBounds) {
                    handleLeft.anchors.horizontalCenterOffset = mouseX - startX
                    //asd.X((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth)
                }
                else {
                    var angle = Math.atan2(mouseY - startY, mouseX - startX)
                    handleLeft.anchors.horizontalCenterOffset = Math.cos(angle) * (joystickLeft.width/ 2)
                    //asd.X((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth)
                }
                break;
            case 3:
                if (fingerOutOfBounds) {
                    handleLeft.anchors.horizontalCenterOffset = mouseX - startX
                    handleLeft.anchors.verticalCenterOffset = mouseY - startY
                    //asd.XY((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth,(handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                } else {
                    var angle = Math.atan2(mouseY - startY, mouseX - startX)
                    handleLeft.anchors.horizontalCenterOffset = Math.cos(angle) * (joystickLeft.width / 2)
                    handleLeft.anchors.verticalCenterOffset = Math.sin(angle) * (joystickLeft.width / 2)
                    //asd.XY((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth,(handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                }
                break;
            default:
                if (fingerOutOfBounds) {
                    handleLeft.anchors.horizontalCenterOffset = mouseX - startX
                    handleLeft.anchors.verticalCenterOffset = mouseY - startY
                    //asd.XY((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth,(handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                } else {
                    var angle = Math.atan2(mouseY - startY, mouseX - startX)
                    handleLeft.anchors.horizontalCenterOffset = Math.cos(angle) * (joystickLeft.width / 2)
                    handleLeft.anchors.verticalCenterOffset = Math.sin(angle) * (joystickLeft.width / 2)
                    //asd.XY((handleLeft.x - joystickLeft.width/2 + handleLeft.height/2)/joystickLeft.bwidth,(handleLeft.y - joystickLeft.height/2 + handleLeft.height/2)/joystickLeft.bheight)
                }
                break;
            }
        }
        onReleased: {
            console.log("1")
            asd.stop();
            returnAnimationLeft.start()
        }
    }
}

