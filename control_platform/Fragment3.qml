import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.2

Rectangle {
    id: anotherWindow
    signal signalExit   // Задаём сигнал
    width:480
    height:320

    // Кнопка для открытия главного окна приложения
    Button {
        text: qsTr("Главное окно")
        width: 180
        height: 50
        anchors.centerIn: parent
        onClicked: {
            anotherWindow.signalExit() // Вызываем сигнал
        }
    }
}
