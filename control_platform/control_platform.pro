TEMPLATE = app

QT += qml quick widgets opengl
#QT += quick
#QT       += declarative
CONFIG += link_pkgconfig
#LIBS +=-lQt5GStreamer-1.0 -lQt5GLib-2.0 -lQt5Core
#LIBS +=-lQt5GStreamerUi-1.0 -lQt5Widgets -lQt5GStreamer-1.0 -lQt5Gui -lQt5GLib-2.0 -lQt5Core

LIBS += -lstreamerVideo
PKGCONFIG += Qt5GStreamer-1.0 Qt5GStreamerQuick-1.0

#  QT += widgets
#LIBS += -L/usr/lib/lib/
#INCLUDEPATH += /usr/include/Qt5GStreamer/

SOURCES += main.cpp \
    move.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)


    LIBS += -L"C:\Android\NDK\toolchains\arm-linux-androideabi-4.8\prebuilt\windows-x86_64\user\lib"
    INCLUDEPATH +=C:\Android\NDK\toolchains\arm-linux-androideabi-4.8\prebuilt\windows-x86_64\user\include

    LIBS += -lqmodbus2

    ANDROID_EXTRA_LIBS += C:\Android\NDK\toolchains\arm-linux-androideabi-4.8\prebuilt\windows-x86_64\user\lib\libqmodbus2.so


DISTFILES += \
    manifest/AndroidManifest.xml \
    manifest/gradle/wrapper/gradle-wrapper.jar \
    manifest/gradlew \
    manifest/res/values/libs.xml \
    manifest/build.gradle \
    manifest/gradle/wrapper/gradle-wrapper.properties \
    manifest/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/manifest

HEADERS += \
    move.h
