#ifndef MULTITOUCHITEM
#define MULTITOUCHITEM

#include <QObject>
#include <QEvent>


class MultiTouchItem : public QDeclarativeItem
{
Q_OBJECT
public:
MultiTouchItem(QDeclarativeItem *parent = 0);
// image painting code omitted
protected:
bool sceneEvent(QEvent *event);
signals:
void pressed();
void released();
};

#endif // MULTITOUCHITEM

