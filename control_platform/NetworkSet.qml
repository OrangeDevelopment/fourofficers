import QtQuick 2.5
import QtQuick.Controls 1.4

Rectangle {

    id: networkSet
    signal signalExit
    visible: false
    color: "darkgray"
    opacity: 0.7
    width: root.width/2
    height: dp(200)
    radius: 7
    anchors.horizontalCenter: parent.horizontalCenter
    Rectangle {
        id: exit_frag1
        x: networkSet.width-exit_frag1.width
        width: dp(40)
        height: dp(40)
        color: button_green_mouse_area.pressed ? "darkred" : "red"
        radius: 7
        Text {
            id: button_green_label
            text: qsTr("Х")
            color: "white"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 16
        }
        MouseArea {
            id: button_green_mouse_area
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                networkSet.signalExit()
            }
        }
    }

    Rectangle {
        width: networkSet.width/1.5
        height: networkSet.height / 5
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.margins: 20
        color: "transparent"
    Rectangle {
        id: selectType
        width: parent.width//networkSet.width/1.5
        height: parent.height//networkSet.height / 5
        color: "transparent"
        Rectangle {
            id: textType
            width: parent.width / 2
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            color: "transparent"
            Text {
                font.pixelSize: dp(18)
                anchors.right: parent.right
                anchors.rightMargin: dp(20)
                anchors.verticalCenter: parent.verticalCenter
                renderType: Text.NativeRendering
                text: "IP адрес"
            }
        }
        Rectangle {
            id: textIp
            width: parent.width/2
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: dp(20)
            color: "transparent"
            TextField {
                id: textIP
                validator:RegExpValidator
                {
                   regExp: /^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$/
                }
                width: parent.width
                height: parent.height
                text: move.getIP()
            }
        }
        Rectangle {
            id: acceptButton
            width: parent.width/3
            height: parent.height/1.5
            anchors.top:selectType.bottom
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            color: areaAccept.pressed ? "#888" : "#fff"
            radius: 7
            Text {
                font.pixelSize: dp(13)
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                renderType: Text.NativeRendering
                text: "Применить"
            }
            MouseArea {
                id: areaAccept
                anchors.fill: parent
                onClicked: {
                    move.setIP(textIP.text)
                    move.init()
                    console.log(move.getIP())
                }
            }
        }
    }
}

}
