#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>

#include "move.h"
//*
#include <cstdlib>
#include <streamerVideo/player.h>
#include <Qt5GStreamer/QGst/Quick/VideoSurface>

#ifndef QMLPLAYER_NO_OPENGL
#include <QGLWidget>
#include <Qt5GStreamer/QGst/Init>
#endif
//*/


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QGst::init(&argc, &argv);
    Control move;
    QQmlApplicationEngine engine;
    QQmlContext *context=engine.rootContext();
//*
    PlayerWeb player;
    player.play();
    context->setContextProperty(QLatin1String("videoSurface1"), &player);

    PlayerRaspberry player2;
    player2.play();
    context->setContextProperty(QLatin1String("videoSurface2"), &player2);
//*/
    context->setContextProperty("move",&move);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
