import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Particles 2.0
import QtQuick.Controls.Styles 1.1

Rectangle {
    id: view
    signal selectView(string choice)

    width: 800//Screen.width//800
    height: 480//Screen.height//480
    readonly property int dpi: Screen.pixelDensity * 25.4
    function dp(x){ return (dpi < 120) ? x : x*(dpi/160); }

    ListModel {
        id: navModel
        ListElement {fragment: "Старт"; choice: "control"}
        ListElement {fragment: "Настройки"; choice: "control"}
        ListElement {fragment: "Выход"; choice: "exit"}
    }

    Rectangle {
        id: mainMenu
        color: "skyblue"
        anchors.fill: parent

        ListView {
            id: menuSelect
            anchors.bottom: parent.bottom
            width: main.width / 3
            anchors.horizontalCenter: parent.horizontalCenter

            height: navModel.count * dp(48)
            anchors.bottomMargin: dp(48)
            delegate: Item {
                height: dp(48)
                anchors.left: parent.left
                anchors.right: parent.right

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: dp(5)
                    color: "whitesmoke"

                    Text {
                        text: fragment
                        anchors.fill: parent
                        font.pixelSize: dp(20)

                        renderType: Text.NativeRendering
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            view.selectView(choice)
                        }
                    }
                }
            }
            model: navModel
        }
    }
}
