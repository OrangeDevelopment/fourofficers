#-------------------------------------------------
#
# Project created by QtCreator 2016-02-25T16:50:41
#
#-------------------------------------------------

QT += qml quick widgets
TARGET   = streamerVideo

TEMPLATE = lib

CONFIG(CLIENT):{
    DEFINES += STREMERVIDEO_LIBRARY

    CONFIG += link_pkgconfig
    contains(QT_VERSION, ^4\\..*) {
      PKGCONFIG += QtGStreamer-1.0 QtGStreamerQuick-1.0
    }
    contains(QT_VERSION, ^5\\..*) {
      PKGCONFIG += Qt5GStreamer-1.0 Qt5GStreamerQuick-1.0
      QT += widgets
    }
    INCLUDEPATH += /usr/include/Qt5GStreamer/
    HEADERS += player.h
    SOURCES += player.cpp
}
CONFIG(SERVER):{
    HEADERS += streamingvideo.h
    SOURCES += streamingvideo.cpp
}

unix {
    target.path = /usr/lib
    headers.files = $$HEADERS
    headers.path = /usr/include/streamerVideo
    INSTALLS += target
    INSTALLS += headers
}

