/*

  .pro
  #######################################
  ...
  LIBS += -lstremerVideo
  CONFIG += link_pkgconfig
  contains(QT_VERSION, ^4\\..*) {
    PKGCONFIG += QtGStreamer-1.0 QtGStreamerUi-1.0
  }
  contains(QT_VERSION, ^5\\..*) {
    PKGCONFIG += Qt5GStreamer-1.0 Qt5GStreamerUi-1.0
    QT += widgets
  }
  ...
  #######################################

  main.cpp
  #######################################
  #ifndef QMLPLAYER_NO_OPENGL
  #include <QGLWidget>
  #include <Qt5GStreamer/QGst/Init>
  #endif

  #include <stremerVideo/player.h>
  ...
  int main(int argc, char **argv)
  {
      QApplication app(argc, argv);

      QGst::init(&argc, &argv);
      QGst::Quick::VideoSurface view;

      PlayerWeb *player = new PlayerWeb(&view);
      player->play();
      view.rootContext()->setContextProperty(QLatin1String("videoSurface"), player);//videoSurface -имя для qml файла
      ...
      view.show();
      return app.exec();
  }
  #######################################

  .qm
  #######################################
  import QtGStreamer 1.0

  ...
  VideoItem {
      id: video
      width: parent.width
      height: parent.height
      surface: videoSurface //bound on the context from main()
  }
  #######################################

 */
#ifndef PLAYER_H
#define PLAYER_H
#include <QtCore/qglobal.h>
#include <QGst/Pipeline>
#include <QGst/Message>
#include <QGlib/Connect>
#include <QGlib/Error>
#include <QGst/ElementFactory>
#include <QGst/Bus>
#include <Qt5GStreamer/QGst/Quick/VideoSurface>

#if defined(STREMERVIDEO_LIBRARY)
#  define STREMERVIDEOSHARED_EXPORT Q_DECL_EXPORT
#else
#  define STREMERVIDEOSHARED_EXPORT Q_DECL_IMPORT
#endif


class STREMERVIDEOSHARED_EXPORT Player : public QGst::Quick::VideoSurface
{
    Q_OBJECT
public:

    explicit Player(QObject *parent = 0);
    virtual ~Player();

    ///
    /// \brief setPort метод изменяет порт приема видео
    /// \param port новое значение порта
    ///
    void setPort(quint32 port);

    ///
    /// \brief getPort возвращает значение порта приема видео
    /// \return возвращает значение порта приема видео
    ///
    quint32 getPort();

    ///
    /// \brief setWidth метод изменяет разрешение видео по длине
    /// \param width новое значение длены
    ///
    void setWidth(quint32 width);

    ///
    /// \brief setHeight метод изменяет разрешение видео по высоте
    /// \param height новое значение всоты
    ///
    void setHeight(quint32 height);

    ///
    /// \brief getWidth метод возвращает разрешение видео по длине
    /// \return возвращает разрешение видео по длине
    ///
    quint32 getWidth();

    ///
    /// \brief getHeight метод возвращает разрешение видео по высоте
    /// \return возвращает разрешение видео по высоте
    ///
    quint32 getHeight();

    ///
    /// \brief play Виртуальная метод начала тронсляции потоковова виео
    ///
    virtual void play(){}

private:

    ///
    /// \brief onBusMessage функция вызвается при критической ошпки работы плейера
    /// \param message сообение ошибки
    ///
    void onBusMessage(const QGst::MessagePtr & message);

protected:

    QGst::PipelinePtr m_pipeline;
    QGst::ElementPtr  m_src;
    QGst::ElementPtr  m_videoconvert;
    QGst::ElementPtr  m_videoSink;
    quint32           m_port;
    quint32           m_width,
                      m_height;
};

class STREMERVIDEOSHARED_EXPORT PlayerWeb : public Player
{
    Q_OBJECT
public:

    explicit PlayerWeb(QObject *parent = 0);

    ///
    /// \brief play метод начала тронсляции потоковова виео
    ///
    void play();

private:

    QGst::ElementPtr m_rtpvrawdepay;
};

class STREMERVIDEOSHARED_EXPORT PlayerRaspberry : public Player
{
    Q_OBJECT
public:

    explicit PlayerRaspberry(QObject *parent = 0);

    ///
    /// \brief play метод начала тронсляции потоковова виео
    ///
    void play();

private:

    QGst::ElementPtr m_rtph264depay;
    QGst::ElementPtr m_avdec_h264;
};

#endif // PLAYER_H
