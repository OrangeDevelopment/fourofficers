#include "player.h"
Player::Player(QObject *parent)
    : QGst::Quick::VideoSurface(parent)
{
    m_pipeline     = QGst::Pipeline::create();
    m_videoSink    = QGst::ElementPtr(this->videoSink());
    m_src          = QGst::ElementFactory::make("udpsrc");
    m_videoconvert = QGst::ElementFactory::make("videoconvert");
    m_width        = 320;
    m_height       = 240;

    m_pipeline->bus()->addSignalWatch();
    QGlib::connect(m_pipeline->bus(), "message::error", this, &Player::onBusMessage);
}

Player::~Player()
{
    if (m_pipeline)
    {
        m_pipeline->setState(QGst::StateNull);
    }
}

void Player::onBusMessage(const QGst::MessagePtr & message)
{
    qCritical() << message.staticCast<QGst::ErrorMessage>()->error();
}

void Player::setPort(quint32 port)
{
    m_port = port;
}

quint32 Player::getPort()
{
    return m_port;
}

void Player::setWidth(quint32 width)
{
    m_width  = width;
}

void Player::setHeight(quint32 height)
{
    m_height = height;
}

quint32 Player::getWidth()
{
    return m_width;
}

quint32 Player::getHeight()
{
    return m_height;
}

PlayerWeb::PlayerWeb(QObject *parent):
    Player(parent)
{
    m_rtpvrawdepay = QGst::ElementFactory::make("rtpvrawdepay");
    m_port = 5000;
}

void PlayerWeb::play()
{
    if (m_pipeline)
    {
        m_pipeline->setState(QGst::StateNull);
    }

    QString caps = "application/x-rtp,"
                   "media=(string)video,"
                   "clock-rate=(int)90000,"
                   "eencoding-name=(string)RAW, "
                   "sampling=(string)YCbCr-4:2:0, "
                   "depth=(string)8, "
                   "width=(string)"  + QString::number(m_width)  + ", "
                   "height=(string)" + QString::number(m_height) + ", "
                   "ssrc=(uint)1825678493, "
                   "payload=(int)96, "
                   "clock-base=(uint)4068866987, "
                   "seqnum-base=(uint)24582";

    m_src->setProperty("port", m_port);
    m_src->setProperty("caps", QGst::Caps::fromString(caps));
    m_src->setProperty("sync", false);
    m_pipeline->add(     m_src);
    m_pipeline->add(     m_rtpvrawdepay);
    m_pipeline->add(     m_videoconvert);
    m_pipeline->add(     m_videoSink);
    m_src->link(         m_rtpvrawdepay);
    m_rtpvrawdepay->link(m_videoconvert);
    m_videoconvert->link(m_videoSink);

    m_pipeline->setState(QGst::StatePlaying);
}

PlayerRaspberry::PlayerRaspberry(QObject *parent):
    Player(parent)
{
    m_rtph264depay = QGst::ElementFactory::make("rtph264depay");
    m_avdec_h264   = QGst::ElementFactory::make("avdec_h264");
    m_port         = 4000;
}

void PlayerRaspberry::play()
{
    if (m_pipeline)
    {
        m_pipeline->setState(QGst::StateNull);
    }

    QString caps = "application/x-rtp, "
                   "media=(string)video, "
                   "clock-rate=(int)90000, "
                   "encoding-name=(string)H264, "
                   "width=(string)"  + QString::number(m_width) + ", "
                   "height=(string)" + QString::number(m_height);

    m_src->setProperty("port", m_port);
    m_src->setProperty("caps", QGst::Caps::fromString(caps));
    m_src->setProperty("sync", false);
    m_pipeline->add(     m_src);
    m_pipeline->add(     m_rtph264depay);
    m_pipeline->add(     m_avdec_h264);
    m_pipeline->add(     m_videoconvert);
    m_pipeline->add(     m_videoSink);
    m_src->link(         m_rtph264depay);
    m_rtph264depay->link(m_avdec_h264);
    m_avdec_h264->link(  m_videoconvert);
    m_videoconvert->link(m_videoSink);

    m_pipeline->setState(QGst::StatePlaying);
}

#include "moc_player.cpp"
