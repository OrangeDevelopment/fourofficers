#ifndef STREAMINGVIDEO_H
#define STREAMINGVIDEO_H
#include <QProcess>
#include <QObject>
#include <QtCore/qglobal.h>

#if defined(STREMERVIDEO_LIBRARY)
#  define STREMERVIDEOSHARED_EXPORT Q_DECL_EXPORT
#else
#  define STREMERVIDEOSHARED_EXPORT Q_DECL_IMPORT
#endif

class STREMERVIDEOSHARED_EXPORT StreamingVideo : public QObject
{
    Q_OBJECT
public:
    StreamingVideo(QObject *parent = 0);
    virtual ~StreamingVideo(){}

    ///
    /// \brief setIp метод изменяет ip-адрес трансляции видео
    /// \param ip
    ///
    void setIp(quint32 ip);

    ///
    /// \brief setIp метод изменяет ip-адрес трансляции видео
    /// \param act1
    /// \param act2
    /// \param act3
    /// \param act4
    ///
    void setIp(quint8 act1, quint8 act2, quint8 act3, quint8 act4);

    ///
    /// \brief getIp
    /// \return
    ///
    const QString getIp();

    ///
    /// \brief setPort метод изменяет порт оправки видео
    /// \param port новый порт
    ///
    void setPort(quint32 port);

    ///
    /// \brief getPort метод возвращает порт оправки видео
    /// \return возвращает порт оправки видео
    ///
    quint32 getPort();

    ///
    /// \brief setWidth метод изменяет разрешение видео по длине
    /// \param width новое значение длены
    ///
    void setWidth(quint32 width);

    ///
    /// \brief setHeight метод изменяет разрешение видео по высоте
    /// \param height новое значение всоты
    ///
    void setHeight(quint32 height);

    ///
    /// \brief setWidth метод изменяет разрешение видео по длине
    /// \param width новое значение длены
    ///
    void setFramerate(quint32 framerate);

    ///
    /// \brief getWidth метод возвращает разрешение видео по длине
    /// \return возвращает разрешение видео по длине
    ///
    quint32 getWidth();

    ///
    /// \brief getHeight метод возвращает разрешение видео по высоте
    /// \return возвращает разрешение видео по высоте
    ///
    quint32 getHeight();

    ///
    /// \brief getHeight метод возвращает разрешение видео по высоте
    /// \return возвращает разрешение видео по высоте
    ///
    quint32 getFramerate();

    ///
    /// \brief run виртуальный метод запускает gstremer в отдельном процессе.
    ///
    virtual void run(){}

private slots:
    ///
    /// \brief onQScenarioFinished Слот вызывается при завершение процесса qscenariocreator.
    /// \param exitstatus код выхода процесса.
    ///
    void onQScenarioFinished(int, QProcess::ExitStatus exitstatus);

    ///
    /// \brief onQScenarioError Слот вызывается при ошибке процесса qscenariocreator.
    /// \param errcode код ошибке процесса.
    ///
    void onQScenarioError(QProcess::ProcessError errcode);

    ///
    /// \brief onReadOutput Слот вызывается при вводе процессом данных в стандартный поток stdout.
    ///
    void onReadOutput();

protected:

    QProcess *m_process;
    QString   m_ip;
    quint32   m_port;
    quint32   m_width,
              m_height,
              m_framerate;
};

class STREMERVIDEOSHARED_EXPORT StreamingVideoWeb : public StreamingVideo
{
    Q_OBJECT
public:
    StreamingVideoWeb(QObject *parent = 0);

    ///
    /// \brief device метод возвращает полный путь к устройству
    /// \param index инекс устройства
    /// \return возвращает полный путь к устройству
    ///
    const QString device(int index);

    ///
    /// \brief sizeDevice метод возвращает полный путь к устройству
    /// \return
    ///
    int sizeDevice();

    ///
    /// \brief currentIndexDevice
    /// \return
    ///
    int currentIndexDevice();

    ///
    /// \brief setCurrentIndexDevice
    /// \param index
    ///
    void setCurrentIndexDevice(int index);

    ///
    /// \brief run метод запускает gstremer в отдельном процессе.
    ///
    void run();

private:
        QStringList m_deviceList;
        int         m_currentDevice;
};

class STREMERVIDEOSHARED_EXPORT StreamingVideoRaspberry : public StreamingVideo
{
    Q_OBJECT
public:
    StreamingVideoRaspberry(QObject *parent = 0);

    ///
    /// \brief run метод запускает gstremer в отдельном процессе.
    ///
    void run();
};

#endif // STREAMINGVIDEO_H
