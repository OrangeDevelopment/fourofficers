#include "streamingvideo.h"
#include <QFileInfo>
#include <QDir>
//#include <QDebug>

StreamingVideo::StreamingVideo(QObject *parent):QObject(parent)
{
    m_process   = new QProcess(this);
    m_ip        = "127.0.0.1";
    m_width     = 320;
    m_height    = 240;
    m_framerate = 30;

    connect(m_process,SIGNAL(finished(int,QProcess::ExitStatus)),this,SLOT(onQScenarioFinished(int,QProcess::ExitStatus)));
    connect(m_process,SIGNAL(error(QProcess::ProcessError)),this, SLOT(onQScenarioError(QProcess::ProcessError)));
    connect(m_process,SIGNAL(readyReadStandardOutput()),this, SLOT(onReadOutput()));
}

void StreamingVideo::onQScenarioFinished(int, QProcess::ExitStatus exitstatus)
{
//    qDebug() << Q_FUNC_INFO << exitstatus;
}

void StreamingVideo::onQScenarioError(QProcess::ProcessError errcode)
{
//    qDebug() << Q_FUNC_INFO << tr("Ошибка работы .\n Код ошибки: %1.").arg(errcode);
}

void StreamingVideo::onReadOutput()
{
//    qDebug() << Q_FUNC_INFO << m_process->readAllStandardOutput();
}

void StreamingVideo::setIp(quint32 ip)
{
    m_ip = QString::number( ip>>24)       + '.' +
           QString::number((ip>>16)%256) + '.' +
           QString::number((ip>>8)%256)  + '.' +
           QString::number( ip%256);
}

void StreamingVideo::setIp(quint8 act1, quint8 act2, quint8 act3, quint8 act4)
{
    m_ip = QString::number(act1) + '.' +
           QString::number(act2) + '.' +
           QString::number(act3) + '.' +
           QString::number(act4);
}

const QString StreamingVideo::getIp()
{
    return m_ip;
}

void StreamingVideo::setPort(quint32 port)
{
    m_port = port;
}

quint32 StreamingVideo::getPort()
{
    return m_port;
}

void StreamingVideo::setWidth(quint32 width)
{
    m_width  = width;
}

void StreamingVideo::setHeight(quint32 height)
{
    m_height = height;
}

void StreamingVideo::setFramerate(quint32 framerate)
{
    m_framerate = framerate;
}

quint32 StreamingVideo::getWidth()
{
    return m_width;
}

quint32 StreamingVideo::getHeight()
{
    return m_height;
}

quint32 StreamingVideo::getFramerate()
{
    return m_framerate;
}

StreamingVideoWeb::StreamingVideoWeb(QObject *parent):
    StreamingVideo(parent)
{
    QFileInfoList fileInfo = QDir("/dev/v4l/by-id/").entryInfoList(QDir::Files | QDir::NoDotAndDotDot);
    foreach(QFileInfo file, fileInfo)
    {
        m_deviceList.push_back(file.absoluteFilePath());
    }
//    qDebug()<<m_deviceList;
    if(m_deviceList.size())
    {
        m_currentDevice = 0;
    }
    else
    {
        m_currentDevice = -1;
    }
    m_port    = 5000;
}

const QString StreamingVideoWeb::device(int index)
{
    if(index < m_deviceList.size())
    {
        return m_deviceList[index];
    }
    else
    {
        return "";
    }
}

int StreamingVideoWeb::sizeDevice()
{
    return m_deviceList.size();
}

int StreamingVideoWeb::currentIndexDevice()
{
    return m_currentDevice;
}

void StreamingVideoWeb::setCurrentIndexDevice(int index)
{
    if(m_deviceList.size() > index)
    {
        m_currentDevice = index;
    }
}

void StreamingVideoWeb::run()
{
    m_process->close();
   QString command = QString("   gst-launch-0.10 "
                     "   v4l2src  device=%1"
                     " ! video/x-raw-yuv,width=%2"
                                       ",height=%3"
                                       ",framerate=%4/1 "
                     " ! ffmpegcolorspace "
                     " ! rtpvrawpay "
                     " ! udpsink host=%5 port=%6 sync=false ").arg(m_deviceList[m_currentDevice])
                                                              .arg(QString::number(m_width))
                                                              .arg(QString::number(m_height))
                                                              .arg(QString::number(m_framerate))
                                                              .arg(m_ip)
                                                              .arg(QString::number(m_port));
     m_process->start(command);
}

StreamingVideoRaspberry::StreamingVideoRaspberry(QObject *parent):
    StreamingVideo(parent)
{
        m_port    = 4000;
}

void StreamingVideoRaspberry::run()
{
    m_process->close();
    QString  command = QString("   gst-launch-1.0 -v "
                       "   rpicamsrc bitrate=1000000 "
                       " ! video/x-h264,width=%1"
                                      ",height=%2"
                                      ",framerate=%3/1 "
                       " ! h264parse "
                       " ! rtph264pay config-interval=10 pt=96 "
                       " ! udpsink host=%5 port=%6 sync=false ").arg(QString::number(m_width))
                                                                .arg(QString::number(m_height))
                                                                .arg(QString::number(m_framerate))
                                                                .arg(m_ip)
                                                                .arg(QString::number(m_port));

    m_process->start(command);
}
